#include <stdio.h>

int get_num(){
char c = getchar();
int a = 0;
int neg = 0;

if(c == '-') {
   neg = 1;
   c = getchar();
}

while(isdigit(c)) {
    a = a*10 + (c - '0');
    c = getchar();
}

if(neg == 1) {
    a = a * -1;
}
return a;
}

int khayam_pascal(int array[][50],int num)
{
    int temp[50];
    int i,j,q,p;

    temp[0] = 1;
    array[0][0] = 1;
    for (i = 1; i < num; i++)
    {
        for (q = 1; q < num; q++)
        {
            array[i][q] = temp[q - 1] + temp[q];
        }
        array[i][0]=1;
        array[i][i] = 1;
        for (p = 0; p <= i; p++)
        {
            temp[p] = array[i][p];
        }
    }
}

int main(){
	int arr[50][50];
	int num=get_num();
	khayam_pascal(arr,num);
	for(int i=0;i<num;i++){
		for(int j=0;j<i+1;j++){
			printf("%d ",arr[i][j]);
		}
		printf("\n");
	}
}
